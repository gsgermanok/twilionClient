<?php
require 'config/path.php';
// Include the Libraries to connect and send message
// using Twil service
require  'vendor/autoload.php';

use Twilio\Rest\Client;

/**
 * This Class must be send a sms to cell phone
 * using twillon API
 */
class Send 
{
  private $sid;
  private $token;

  public function __construct() {
    $ini = parse_ini_file('config/twilio.ini');
    $this->sid = $ini['ACCOUNT_SID'];
    $this->token = $ini['AUTH_TOKE'];
  }


  public function send($from, $to, $message) {
    $client = new Client($this->sid, $this->token);
    $client->messages->create(
      $to,
      array(
        'from' => $from,
        'body' => $message
      )
    );
    return true;
  }

  public function getSid() {
    return $this->sid;
  }
  public function getToken() {
    return $this->token;
  }
}
