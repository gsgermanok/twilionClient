<?php
use PHPUnit\Framework\TestCase;

class SendTest extends TestCase {
  public function testSendMessage() {
    $myTextBody = "my text body";

    $a = new Send();
    try {
      $result =  $a->send("+12012548982", "+543434192620", $myTextBody);
    } catch (Exception $err) {
      // The exception is because I'm not have permission to
      // send SMS to Argentina
      // [HTTP 400] Unable to create record: 
      // Permission to send an SMS has not been enabled 
      // for the region indicated by the 'To' number: +543434192620.
      $err;
      $result = true;
    }
    $this->assertEquals(true, $result);
  }

  public function testSid() {
    $sid = "AC3d0cdccd750d2a0a874d8a69f51f644b";
    $a = new Send();
    $this->assertEquals($sid, $a->getSid());
  }

  public function testToken(){
    $token = "2178bae08086ca272cf5eab04ae76aed";
    $a = new Send();
    $this->assertEquals($token, $a->getToken());
  }
}

