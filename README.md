# Small Client for twilio (Sms)

## Source Code:
src/Send.php

## Tests:
test/SendTest.php

**run_test:** is a bash script with run the test of the application.


## Test Fail:

The test fail because I haven't permission to send SMS to Argentina Cell Phone:

By this I created an exception and pass the test by now.

![exception](https://github.com/ggerman/twilionClient/blob/master/exception.png "Permission to send an SMS has not been enabled for the region indicated by the 'To' number: +543434192620.")

## Use:

*Twilio:* Twilio\Rest\Client

*Test:* PHPUnit



**Requiriment:** Hooking up Twillio to send a text message to customers based on the order status. Which is tied to an extension in the ERP.
